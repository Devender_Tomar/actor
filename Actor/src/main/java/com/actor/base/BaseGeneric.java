package com.actor.base;

import java.io.Serializable;
import java.util.List;

public interface BaseGeneric<E>
{


	public void saveOrUpdate(E entity);
	Serializable save(E entity);

	public void merge(E entity);
	boolean deleteById(Class<?> type, Serializable id);

	void deleteAll();

	List<E> findAll();

	E findById(Serializable id);




	
}
