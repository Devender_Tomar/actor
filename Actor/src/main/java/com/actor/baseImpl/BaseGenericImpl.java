package com.actor.baseImpl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;

import com.actor.base.BaseGeneric;

public class BaseGenericImpl <E> implements BaseGeneric<E>{
	@PersistenceContext
	private EntityManager entityManager;
	
	private final Class<E> entityClass;
	public Session session;
	
	@Override
	public Serializable save(E entity) {
		session=getSession();
		Serializable s = session.save(entity);
		return s;
	}


	@Override
	public void merge(E entity) {
		session = getSession();
		session.persist(entity);
	}




	@SuppressWarnings("unchecked")
	public BaseGenericImpl() {
	this.entityClass=(Class<E>)((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	public Session getSession()
	{
		try {
			session=this.entityManager.unwrap(Session.class);	
			if(session.getTransaction().isActive())
				return session;
			
		}catch (Exception e) {
			System.err.println("Showing error in BaseGenericImpl class");
			e.printStackTrace();
		}
		 session.beginTransaction();
		 return session;
	}


	@Override
	public void saveOrUpdate(E entity) {
		session=getSession();
		session.saveOrUpdate(entity);
	}

	@Override
	public boolean deleteById(Class<?> type, Serializable id) {
		
		try {
			session=getSession();
			Object persist=session.get(type, id);
			session.delete(persist);	
		} catch (Exception e) {
			e.printStackTrace();
			return false;
			
		}
		
		return true;
	}

	@Override
	public void deleteAll() {
		session=getSession();
		List<E> list=findAll();
		for (E e : list) {
			session.delete(e);
		}
		
	}

	@Override
	public List<E> findAll() {
		session=getSession();
	return session.createCriteria(this.entityClass).list();
	
	}

	@Override
	public E findById(Serializable id) {
		session=getSession();
		E e= session.get(this.entityClass, id);
		return e;
	}

}
