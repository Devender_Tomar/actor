package com.actor.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.actor.dto.ActorModelDTO;
import com.actor.dto.MovieModelDTO;
import com.actor.service.ActorService;

@RestController
@RequestMapping("actor")
public class ActorController {

	@Autowired
	ActorService actorService;
	
	@PostMapping("create")
	ActorModelDTO saveOrUpdate(@RequestBody ActorModelDTO actorModelDTO)
	{
		return	actorService.saveOrUpdate(actorModelDTO);
	}
	
	@DeleteMapping("deleteByid")
	public boolean deleteById(@RequestHeader(defaultValue = "", value = "actor_id")String id)
	{
		return actorService.deleteById(id);
	}
	
	@GetMapping("findById")
	ActorModelDTO findById(@RequestHeader(defaultValue = "", value = "actor_id")String id)
	{
		return actorService.findById(id);
	}
	@GetMapping("findAll")
	List<ActorModelDTO> findAll()
	{
		return actorService.findAll();
	}
	@GetMapping("findByActorName")
	List<MovieModelDTO> findByActorName(@RequestHeader(defaultValue = "", value = "actor_name")String name)
	{
		List<MovieModelDTO> list=actorService.findByActorName(name);
		return list;
	}
	
	@DeleteMapping("deleteAll")
	public void deleteAll()
	{
	 actorService.deleteAll();
	}
}
