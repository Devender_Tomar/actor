package com.actor.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.actor.dto.GenreModelDTO;
import com.actor.service.GenreService;

@RestController
@RequestMapping("genre")
public class GenreController {

	@Autowired
	GenreService genreService;
	
	@PostMapping("create")
	GenreModelDTO saveOrUpdate(@RequestBody GenreModelDTO genreModelDTO)
	{
		return	genreService.saveOrUpdate(genreModelDTO);
	}
	
	@DeleteMapping("deleteByid")
	public boolean deleteById(@RequestHeader(defaultValue = "", value = "genre_id") String id)
	{
		return genreService.deleteById(id);
	}
	
	@GetMapping("findById")
	GenreModelDTO findById(@RequestHeader(defaultValue = "", value = "genre_id") String id)
	{
		return genreService.findById(id);
	}
	@GetMapping("findAll")
	List<GenreModelDTO> findAll()
	{
		return genreService.findAll();
	}
	
	
	
	@DeleteMapping("deleteAll")
	public void deleteAll()
	{
	 genreService.deleteAll();
	}
}
