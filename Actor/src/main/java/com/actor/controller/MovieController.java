package com.actor.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.actor.dto.MovieModelDTO;
import com.actor.service.MovieService;

@RestController
@RequestMapping("movie")
public class MovieController 
{
	@Autowired
	MovieService movieService;
	
	@PostMapping("create")
	MovieModelDTO saveOrUpdate(@RequestBody MovieModelDTO MovieModelDTO)
	{
		return	movieService.saveOrUpdate(MovieModelDTO);
	}
	
	@DeleteMapping("deleteByid")
	public boolean deleteById(@RequestHeader(defaultValue = "", value = "movie_id") String id)
	{
		return movieService.deleteById(id);
	}
	
	@GetMapping("findById")
	MovieModelDTO findById(@RequestHeader(defaultValue = "", value = "movie_id")String id)
	{
		return movieService.findById(id);
	}
	@GetMapping("findAll")
	List<MovieModelDTO> findAll()
	{
		return movieService.findAll();
	}
	
	@GetMapping("getMovieByGenre")
	List<MovieModelDTO> findByGenre(@RequestHeader(defaultValue = "", value = "genre_type")String genre)
	{
		return movieService.findByGenre(genre);
	}
	
	@DeleteMapping("deleteAll")
	public void deleteAll()
	{
	 movieService.deleteAll();
	}
	
}
