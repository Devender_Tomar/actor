package com.actor.dao;

import java.util.List;

import com.actor.base.BaseGeneric;
import com.actor.model.ActorModel;
import com.actor.model.MovieModel;

public interface ActorDao extends BaseGeneric<ActorModel> 
{
	List<MovieModel> getMovieByActor(String name);
}
