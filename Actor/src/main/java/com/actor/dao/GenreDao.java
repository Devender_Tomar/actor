package com.actor.dao;

import com.actor.base.BaseGeneric;
import com.actor.model.GenreModel;

public interface GenreDao extends  BaseGeneric<GenreModel>{

}
