package com.actor.dao;

import java.util.List;

import com.actor.base.BaseGeneric;
import com.actor.model.MovieModel;

public interface MovieDao extends BaseGeneric<MovieModel>{

}
