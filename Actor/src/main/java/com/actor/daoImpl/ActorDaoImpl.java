package com.actor.daoImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.actor.baseImpl.BaseGenericImpl;
import com.actor.dao.ActorDao;
import com.actor.dao.MovieDao;
import com.actor.model.ActorModel;
import com.actor.model.MovieModel;


@Repository
public class ActorDaoImpl extends BaseGenericImpl<ActorModel> implements ActorDao{

	@Autowired
	MovieDao moviedao;
	
	@Override
	public List<MovieModel> getMovieByActor(String name) {
		List<MovieModel> list=moviedao.findAll();
		List<MovieModel> lModel=new ArrayList<MovieModel>();
		for (MovieModel movieModel : list) {
			if(movieModel.getActorModel().getFirst_name().equals(name))
				lModel.add(movieModel);
			
		}
		
		return lModel;
	}

}