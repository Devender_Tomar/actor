package com.actor.daoImpl;

import org.springframework.stereotype.Repository;

import com.actor.baseImpl.BaseGenericImpl;
import com.actor.dao.GenreDao;
import com.actor.model.GenreModel;

@Repository
public class GenreDaoImpl extends BaseGenericImpl<GenreModel> implements GenreDao 
{

}
