package com.actor.daoImpl;

import org.springframework.stereotype.Repository;

import com.actor.baseImpl.BaseGenericImpl;
import com.actor.dao.MovieDao;
import com.actor.model.MovieModel;

@Repository
public class MovieDaoImpl extends BaseGenericImpl<MovieModel> implements MovieDao
{

}
