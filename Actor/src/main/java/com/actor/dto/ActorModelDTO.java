package com.actor.dto;

import java.util.Date;
import java.util.List;

import com.actor.model.MovieModel;

public class ActorModelDTO {

	public String actor_id;
	public String first_name;
	public String last_name;
	public String gender;
	public String mother_name;
	public String father_name;
	public Date birthday_date;
	public String birthPlace;
	public String phone_no;
	public String email;
	public String nationality;
	public MovieModelDTO moviemodeldto;

	public MovieModelDTO getMoviemodeldto() {
		return moviemodeldto;
	}

	public void setMoviemodeldto(MovieModelDTO moviemodeldto) {
		this.moviemodeldto = moviemodeldto;
	}

	public String getActor_id() {
		return actor_id;
	}

	public void setActor_id(String actor_id) {
		this.actor_id = actor_id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMother_name() {
		return mother_name;
	}

	public void setMother_name(String mother_name) {
		this.mother_name = mother_name;
	}

	public String getFather_name() {
		return father_name;
	}

	public void setFather_name(String father_name) {
		this.father_name = father_name;
	}

	public Date getBirthday_date() {
		return birthday_date;
	}

	public void setBirthday_date(Date birthday_date) {
		this.birthday_date = birthday_date;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	public String getPhone_no() {
		return phone_no;
	}

	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public ActorModelDTO(String actor_id, String first_name, String last_name, String gender, String mother_name,
			String father_name, Date birthday_date, String birthPlace, String phone_no, String email,
			String nationality) {
		super();
		this.actor_id = actor_id;
		this.first_name = first_name;
		this.last_name = last_name;
		this.gender = gender;
		this.mother_name = mother_name;
		this.father_name = father_name;
		this.birthday_date = birthday_date;
		this.birthPlace = birthPlace;
		this.phone_no = phone_no;
		this.email = email;
		this.nationality = nationality;
	}

	public ActorModelDTO() {
		super();
	}

}
