package com.actor.dto;

public class GenreModelDTO {

	public String genre_id;
	public String type;
	public MovieModelDTO movieModelDTO;
	public MovieModelDTO getMovieModelDTO() {
		return movieModelDTO;
	}
	public void setMovieModelDTO(MovieModelDTO movieModelDTO) {
		this.movieModelDTO = movieModelDTO;
	}
	public String getGenre_id() {
		return genre_id;
	}
	public void setGenre_id(String genre_id) {
		this.genre_id = genre_id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
		
	
}

