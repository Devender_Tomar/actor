package com.actor.dto;

public class MovieModelDTO {

	private String movie_id;
	private String movie_name;
	private String year;
	private String description;
	public GenreModelDTO genreModelDTO;
	public ActorModelDTO actorModelDTO;
	public String getMovie_id() {
		return movie_id;
	}
	public void setMovie_id(String movie_id) {
		this.movie_id = movie_id;
	}
	public String getMovie_name() {
		return movie_name;
	}
	public void setMovie_name(String movie_name) {
		this.movie_name = movie_name;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public GenreModelDTO getGenreModelDTO() {
		return genreModelDTO;
	}
	public void setGenreModelDTO(GenreModelDTO genreModelDTO) {
		this.genreModelDTO = genreModelDTO;
	}
	public ActorModelDTO getActorModelDTO() {
		return actorModelDTO;
	}
	public void setActorModelDTO(ActorModelDTO actorModelDTO) {
		this.actorModelDTO = actorModelDTO;
	}
	
	public MovieModelDTO() {
		super();
	}
	
	
	
}
