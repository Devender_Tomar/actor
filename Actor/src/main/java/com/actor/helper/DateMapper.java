package com.actor.helper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateMapper {
	HelperExtension helperExtension = new HelperExtension();

	public long dateToLong(Date date) {
		if (helperExtension.isNullOrEmpty(date)) {
			return 0;
		}
		return date.getTime();
	}

	public Date longToDate(long date) {
		if (date == 0) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.setTimeZone(TimeZone.getDefault());
		return new Date(date);
	}

	public byte[] stringToByte(String data) {
		if (helperExtension.isNullOrEmpty(data)) {
			return null;
		}
		return data.getBytes();
	}

	public String byteToString(byte[] data) {
		if (helperExtension.isNullOrEmpty(data)) {
			return "";
		}
		return new String(data);
	}
	/*
	 * public String booleantoString(boolean flag) { if(flag==true) return "true";
	 * else return "false"; } public boolean Stringtoboolean(String str) {
	 * if(str=="true") return true; else return false; }
	 */
}