package com.actor.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.actor.dto.ActorModelDTO;
import com.actor.dto.MovieModelDTO;
import com.actor.helper.DateMapper;
import com.actor.model.ActorModel;
import com.actor.model.MovieModel;

@Mapper(uses = DateMapper.class)
public interface ActorMapper {

	ActorMapper INSTANCE=Mappers.getMapper(ActorMapper.class);
	
	@Mapping(source = "actor_id", target = "actor_id")
	ActorModelDTO modelToDTO(ActorModel actorModel);
	
	@InheritInverseConfiguration
	ActorModel dtoToModel(ActorModelDTO actorModelDTO);
	
	
}
