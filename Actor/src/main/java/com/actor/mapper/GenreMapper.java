package com.actor.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.actor.dto.GenreModelDTO;
import com.actor.helper.DateMapper;
import com.actor.model.GenreModel;

@Mapper
public interface GenreMapper {

	GenreMapper INSTANCE=Mappers.getMapper(GenreMapper.class);
	
	@Mapping(source = "genre_id", target = "genre_id")
	GenreModelDTO modelToDTO(GenreModel genreModel);
	
	@InheritInverseConfiguration
	GenreModel dtoToModel(GenreModelDTO dto);
}
