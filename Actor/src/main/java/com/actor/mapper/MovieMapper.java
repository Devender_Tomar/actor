package com.actor.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.actor.dto.MovieModelDTO;
import com.actor.model.MovieModel;

@Mapper
public interface MovieMapper {

	MovieMapper INSTANCE=Mappers.getMapper(MovieMapper.class);
	
	
	@Mappings({@Mapping(source = "genreModel", target = "genreModelDTO"),@Mapping(source = "actorModel", target = "actorModelDTO")})
	MovieModelDTO modelToDTO(MovieModel model);
	
	@InheritInverseConfiguration
	MovieModel dtoToModel(MovieModelDTO dto);
	
}
