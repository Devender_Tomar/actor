package com.actor.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "actor")
public class ActorModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4326896225784893967L;

	public List<MovieModel> getMoviemodel() {
		return moviemodel;
	}

	public void setMoviemodel(List<MovieModel> moviemodel) {
		this.moviemodel = moviemodel;
	}

	@OneToMany(mappedBy = "actorModel")
	List<MovieModel> moviemodel = new ArrayList<MovieModel>();

	@Id
	@Column(name = "actor_id",  unique = true, length = 20)
	private String actor_id;

	@Column(name = "first_name", length = 45)
	private String first_name;

	@Column(name = "last_name", length = 45)
	private String last_name;

	@Column(name = "gender", length = 45)
	private String gender;

	@Column(name = "mother_name", length = 45)
	private String mother_name;

	@Column(name = "father_name", length = 45)
	private String father_name;

	@Column(name = "birthday_date", length = 45)
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date birthday_date;

	@Column(name = "birthPlace", length = 45)
	private String birthPlace;

	@Column(name = "phone_no", length = 45)
	private String phone_no;

	@Column(name = "email", length = 45)
	private String email;

	@Column(name = "nationality", length = 45)
	private String nationality;

	public String getActor_id() {
		return actor_id;
	}

	public void setActor_id(String actor_id) {
		this.actor_id = actor_id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMother_name() {
		return mother_name;
	}

	public void setMother_name(String mother_name) {
		this.mother_name = mother_name;
	}

	public String getFather_name() {
		return father_name;
	}

	public void setFather_name(String father_name) {
		this.father_name = father_name;
	}

	public Date getBirthday_date() {
		return birthday_date;
	}

	public void setBirthday_date(Date birthday_date) {
		this.birthday_date = birthday_date;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	public String getPhone_no() {
		return phone_no;
	}

	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

}
