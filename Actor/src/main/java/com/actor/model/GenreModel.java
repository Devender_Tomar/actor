package com.actor.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "genre")
public class GenreModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4960183686347592157L;

	@Id
	@Column(name = "genre_id", nullable = false, unique = true, length = 20, insertable = true)
	private String genre_id;

	@Column(name = "type")
	private String type;

	@OneToMany(mappedBy = "genreModel")
	private List<MovieModel> movieModels;

	public String getGenre_id() {
		return genre_id;
	}

	public void setGenre_id(String genre_id) {
		this.genre_id = genre_id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
	public List<MovieModel> getMovieModels() {
		return movieModels;
	}

	public void setMovieModels(List<MovieModel> movieModels) {
		this.movieModels = movieModels;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
