package com.actor.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "movie")
public class MovieModel implements Serializable {

	private static final long serialVersionUID = -4434892933805454060L;

	@Id
	@Column(name = "movie_id", unique = true, nullable = false, length = 20)
	private String movie_id;

	@Column(name = "movie_name", length = 45)
	private String movie_name;

	@Column(name = "year", length = 45)
	private String year;

	@Column(name = "description", length = 45)
	private String description;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "genre_id")
	private GenreModel genreModel;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "actor_id")
	private ActorModel actorModel;

	public String getMovie_id() {
		return movie_id;
	}

	public void setMovie_id(String movie_id) {
		this.movie_id = movie_id;
	}

	public String getMovie_name() {
		return movie_name;
	}

	public void setMovie_name(String movie_name) {
		this.movie_name = movie_name;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	

	public GenreModel getGenreModel() {
		return genreModel;
	}

	public void setGenreModel(GenreModel genreModel) {
		this.genreModel = genreModel;
	}

	public ActorModel getActorModel() {
		return actorModel;
	}

	public void setActorModel(ActorModel actorModel) {
		this.actorModel = actorModel;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public MovieModel() {
		super();
	}

}
