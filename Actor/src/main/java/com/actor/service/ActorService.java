package com.actor.service;

import java.util.List;

import com.actor.dto.ActorModelDTO;
import com.actor.dto.MovieModelDTO;

public interface ActorService {

	ActorModelDTO saveOrUpdate(ActorModelDTO actorModelDTO);
	public boolean deleteById(String id);
	ActorModelDTO findById(String id);
	List<ActorModelDTO> findAll();
	public void deleteAll();
	List<MovieModelDTO> findByActorName(String name);
	
}
