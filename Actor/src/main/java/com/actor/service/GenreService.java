package com.actor.service;

import java.util.List;
import com.actor.dto.GenreModelDTO;

public interface GenreService {


	GenreModelDTO saveOrUpdate(GenreModelDTO genreModelDTO);
	public boolean deleteById(String id);
	GenreModelDTO findById(String id);
	List<GenreModelDTO> findAll();
	public void deleteAll();
}
