package com.actor.service;

import java.util.List;
import com.actor.dto.MovieModelDTO;

public interface MovieService {


	MovieModelDTO saveOrUpdate(MovieModelDTO movieModelDTO);
	public boolean deleteById(String id);
	MovieModelDTO findById(String id);
	List<MovieModelDTO> findAll();
	public void deleteAll();
	List<MovieModelDTO> findByGenre(String genre);
}
