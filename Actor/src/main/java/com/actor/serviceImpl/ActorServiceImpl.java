package com.actor.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.actor.dao.ActorDao;
import com.actor.dto.ActorModelDTO;
import com.actor.dto.MovieModelDTO;
import com.actor.mapper.ActorMapper;
import com.actor.mapper.MovieMapper;
import com.actor.model.ActorModel;
import com.actor.model.MovieModel;
import com.actor.service.ActorService;

@Service
@Transactional(readOnly = true)
public class ActorServiceImpl implements ActorService{

	@Autowired
	ActorDao actorDao;
	
	@Override
	@Transactional(readOnly = false)
	public ActorModelDTO saveOrUpdate(ActorModelDTO actorModelDTO) {
		
		String act_id ="act_id"+new Date().getTime();
		ActorModel actorModel=new ActorModel();
		
		if(actorModelDTO.getActor_id()==null)
		{
			actorModelDTO.setActor_id(act_id);
		}
		
		actorModel =ActorMapper.INSTANCE.dtoToModel(actorModelDTO);
		actorDao.saveOrUpdate(actorModel);
		return actorModelDTO;
		
	}

	@Override
	@Transactional(readOnly = false)
	public boolean deleteById(String id) {
		 return actorDao.deleteById(ActorModel.class, id);

	}

	@Override
	public ActorModelDTO findById(String id) {
	
		ActorModel actorModel= actorDao.findById(id);
		ActorModelDTO actorModelDTO=ActorMapper.INSTANCE.modelToDTO(actorModel);
		return actorModelDTO;
	}

	@Override
	public List<ActorModelDTO> findAll() {
		
	List<ActorModel> modellist=actorDao.findAll();
	List<ActorModelDTO> dtolist=new ArrayList<ActorModelDTO>();
	for (ActorModel actorModel : modellist) {
		ActorModelDTO actorModelDTO=ActorMapper.INSTANCE.modelToDTO(actorModel);
		dtolist.add(actorModelDTO);
	}
		return dtolist;
	}

	@Override
	@Transactional(readOnly = false)
	public void deleteAll()
	{
		actorDao.deleteAll();
		
	}

	@Override
	public List<MovieModelDTO> findByActorName(String name) {
		List<MovieModel> model=actorDao.getMovieByActor(name);
		List<MovieModelDTO> movieModelDTO=new ArrayList<MovieModelDTO>();
		for (MovieModel movieModel : model) {
			MovieModelDTO movieModeldto=MovieMapper.INSTANCE.modelToDTO(movieModel);
			movieModelDTO.add(movieModeldto);
		}
		return movieModelDTO;
	}

}
