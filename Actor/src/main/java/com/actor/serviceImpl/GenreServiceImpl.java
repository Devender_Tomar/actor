package com.actor.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.actor.dao.GenreDao;
import com.actor.dto.GenreModelDTO;
import com.actor.mapper.GenreMapper;
import com.actor.model.GenreModel;
import com.actor.service.GenreService;

@Service
@Transactional(readOnly = true)
public class GenreServiceImpl implements GenreService {

	@Autowired
	GenreDao genredao;

	@Override
	@Transactional(readOnly = false)
	public GenreModelDTO saveOrUpdate(GenreModelDTO genreModelDTO) {

		String genreid = "gen" + new Date().getTime();

		GenreModel genreModel = new GenreModel();
		if (genreModelDTO.getGenre_id() == null) {
			genreModelDTO.setGenre_id(genreid);
		}
		genreModel = GenreMapper.INSTANCE.dtoToModel(genreModelDTO);
		genredao.saveOrUpdate(genreModel);
		return genreModelDTO;
	}

	@Override
	@Transactional(readOnly = false)
	public boolean deleteById(String id) {
		return genredao.deleteById(GenreModel.class, id);

	}

	@Override
	public GenreModelDTO findById(String id) {
		GenreModel genreModel = genredao.findById(id);
		GenreModelDTO dto = GenreMapper.INSTANCE.modelToDTO(genreModel);
		return dto;
	}

	@Override
	public List<GenreModelDTO> findAll() {
		List<GenreModel> models = genredao.findAll();
		List<GenreModelDTO> dtos=new ArrayList<GenreModelDTO>();
		for (GenreModel genreModel: models) {
			GenreModelDTO dto=GenreMapper.INSTANCE.modelToDTO(genreModel);
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	@Transactional(readOnly = false)
	public void deleteAll() {
		genredao.deleteAll();

	}

}
