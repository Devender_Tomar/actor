package com.actor.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.actor.dao.ActorDao;
import com.actor.dao.GenreDao;
import com.actor.dao.MovieDao;
import com.actor.dto.ActorModelDTO;
import com.actor.dto.GenreModelDTO;
import com.actor.dto.MovieModelDTO;
import com.actor.mapper.ActorMapper;
import com.actor.mapper.GenreMapper;
import com.actor.mapper.MovieMapper;
import com.actor.model.ActorModel;
import com.actor.model.GenreModel;
import com.actor.model.MovieModel;
import com.actor.service.MovieService;

@Service
@Transactional(readOnly = true)
public class MovieServiceImpl implements MovieService {

	@Autowired
	ActorDao actordao; 
	
	@Autowired
	GenreDao genredao;
	
	@Autowired
	MovieDao moviedao;
	
	@Override
	@Transactional(readOnly = false)
	public MovieModelDTO saveOrUpdate(MovieModelDTO movieModelDTO) {
		MovieModel model=new MovieModel();
		ActorModel actorModel=new ActorModel();
		ActorModelDTO actorModelDTO=new ActorModelDTO();
		GenreModel genreModel=new GenreModel();
		GenreModelDTO genreModelDTO=new GenreModelDTO();
		String mov_id="mov_"+new Date().getTime();
		
		if(movieModelDTO.getMovie_id()==null)
		{
			movieModelDTO.setMovie_id(mov_id);
		}
		if(movieModelDTO.getActorModelDTO().getActor_id()!=null)
		{
			actorModel=actordao.findById(movieModelDTO.getActorModelDTO().getActor_id());
			actorModelDTO=ActorMapper.INSTANCE.modelToDTO(actorModel);
			movieModelDTO.setActorModelDTO(actorModelDTO);
		}
		if(movieModelDTO.getGenreModelDTO().getGenre_id()!=null)
		{
			genreModel=genredao.findById(movieModelDTO.getGenreModelDTO().getGenre_id());
			genreModelDTO=GenreMapper.INSTANCE.modelToDTO(genreModel);
			movieModelDTO.setGenreModelDTO(genreModelDTO);
		}
		
		model=MovieMapper.INSTANCE.dtoToModel(movieModelDTO);
		moviedao.merge(model);
		return movieModelDTO;
	}

	@Override
	@Transactional(readOnly = false)
	public boolean deleteById(String id) {
		try {
			moviedao.deleteById(MovieModel.class, id);
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public MovieModelDTO findById(String id) {
		MovieModel model= moviedao.findById(id);
		MovieModelDTO movieModelDTO=MovieMapper.INSTANCE.modelToDTO(model);
		return movieModelDTO;
	}

	@Override
	public List<MovieModelDTO> findAll() {
		List<MovieModelDTO> dtos=new ArrayList<MovieModelDTO>();
		List<MovieModel> models=moviedao.findAll();
		for (MovieModel movieModel : models) {
			MovieModelDTO movieModelDTO=MovieMapper.INSTANCE.modelToDTO(movieModel);
			dtos.add(movieModelDTO);
		}
		return dtos;
	}

	@Override
	@Transactional(readOnly = false)
	public void deleteAll() {
			
		}

	@Override
	public List<MovieModelDTO> findByGenre(String genre) {
		List<MovieModel> model=moviedao.findAll();
		List<MovieModelDTO> modelDTOs =new ArrayList<>();
		for (MovieModel movieModel : model) {
			if(movieModel.getGenreModel().getType().equals(genre))
			{
				MovieModelDTO dto=MovieMapper.INSTANCE.modelToDTO(movieModel);
				modelDTOs.add(dto);
			}

			
		}
		
		
		
		return modelDTOs;
	}
		
	

	
}
